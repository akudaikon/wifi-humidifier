var emptyCalValue = document.getElementById('emptyCalValue'),
    fullCalValue = document.getElementById('fullCalValue'),
    mdnsName = document.getElementById('mdnsName'),
    mqttEnable = document.getElementById('mqttEnable'),
    mqttServer = document.getElementById('mqttServer'),
    mqttUser = document.getElementById('mqttUser'),
    mqttPassword = document.getElementById('mqttPassword');

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = JSON.parse(xhttp.responseText);
        emptyCalValue.innerHTML = settings.waterEmpty;
        fullCalValue.innerHTML = settings.waterFull;
        mdnsName.value = settings.mdnsName;
        mqttServer.value = settings.mqttServer;
        mqttUser.value = settings.mqttUser;
        mqttPassword.value = settings.mqttPassword;
        mqttEnable.checked = settings.mqttEnable;
      }
    }
  };
  xhttp.open("GET", "getSettings", true);
  xhttp.send();
}

var iter = 0;

doCalib = function(type) {
  var xhttp = new XMLHttpRequest();
  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-time'></span></h1>";

  if (type == 'empty')
  {
    if (confirm("Ensure water bin is empty, then click OK to start calibration."))
    {
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
          if (xhttp.status == 200) {
            getSettings();
          }
        }
      };
      xhttp.open("GET", "calib?empty", false);
      xhttp.send();
    }
  }
  else if (type == 'full')
  {
    if (confirm("Ensure water bin filled fully, then click OK to start calibration."))
    {
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
          if (xhttp.status == 200) {
            getSettings();
          }
        }
      };
      xhttp.open("GET", "calib?full", false);
      xhttp.send();
    }
  }
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);