getStatus = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var status = JSON.parse(xhttp.responseText);

        document.getElementById("humidity").innerHTML = status.humidity + "%";

        if (status.fanAuto) document.getElementById("fanSpeed").innerHTML = "Auto (" + status.fanSpeed + ")";
        else document.getElementById("fanSpeed").innerHTML = status.fanSpeed;

        document.getElementById("waterPercent").innerHTML = status.waterPercent + "%";
        
        document.getElementById("waterPercentBar").style = "width: " + status.waterPercent + "%";

        document.getElementById("waterPercentBar").className = "progress-bar";
        if (status.waterPercent > 50) document.getElementById("waterPercentBar").classList.add("progress-bar-success");
        else if (status.waterPercent > 25) document.getElementById("waterPercentBar").classList.add("progress-bar-warning");
        else document.getElementById("waterPercentBar").classList.add("progress-bar-danger");

        setTimeout(getStatus, 5000);
      }
    }
  };

  xhttp.open("GET", "getStatus", true);
  xhttp.send();
};

// To get around onLoad not firing on back
setTimeout(getStatus, 100);
