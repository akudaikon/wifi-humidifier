#ifndef AM2315
#define AM2315

#define AM2315_I2C_ADDR 0x5C
#define AM2315_READREG  0x03

bool AM2315_readData();
float AM2315_getHumidity();
float AM2315_getTemperature();

#endif