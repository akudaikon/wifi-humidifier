#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <FS.h>
#include <Ticker.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/Picopixel.h>
#include <Fonts/TomThumb.h>
#include <SparkLine.h>
#include <VL53L0X.h>
#include <Adafruit_Si7021.h>
//#include "am2315.h"
#include "sprites.h"

#define PIN_I2C_SCL             D1
#define PIN_I2C_SDA             D2
#define PIN_FAN_MOSFET          D5
#define PIN_HUMID_BUTTON        D6  // active high
#define PIN_FAN_BUTTON          D7  // active high

#define FAN_RAMP_STEP_TIME      2000
#define BUTTON_POLL_TIME        10
#define DISPLAY_JUMBLE_TIME     900000
#define MQTT_UPDATE_TIME        300000
#define BUTTON_LONG_PRESS_TIME  3000

#define NUM_FAN_SPEEDS          6
#define NUM_HUMIDITY_SETPOINTS  9

#define WATER_LEVEL_DEBOUNCE    10
#define LOW_WATER_WARNING       15  // percent

#define SETTINGS_REV            0xA1
#define MAX_STRING_LENGTH       128

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_MQTT_ENABLE,
  SETTING_HUMIDITY_SETPOINT,
  // uint16
  SETTING_WATER_EMPTY,
  SETTING_WATER_FULL = SETTING_WATER_EMPTY + 2,
  SETTING_DELIM = SETTING_WATER_FULL + 2,
  // Max 128 characters for EEPROM stored strings below...
  SETTING_MDNS_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  SETTING_MQTT_HUMIDITY_TOPIC_LENGTH = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH,
  SETTING_MQTT_HUMIDITY_TOPIC,
  NUM_OF_SETTINGS = SETTING_MQTT_HUMIDITY_TOPIC + MAX_STRING_LENGTH
};

enum displayLayouts
{
  //DISPLAY_ERROR,
  DISPLAY_LAYOUT_1,
  DISPLAY_LAYOUT_2,
  NUM_DISPLAY_LAYOUTS
};

const uint16_t fanSpeeds[NUM_FAN_SPEEDS] = { 0, 64, 128, 192, 256, 1023 };
const uint8_t humiditySetpoints[NUM_HUMIDITY_SETPOINTS] = { 25, 30, 35, 40, 45, 50, 55, 60, 65 };
const uint16_t airManAnimSpeeds[NUM_FAN_SPEEDS] = { 0, 500, 250, 150, 100, 50 };

String mdnsName;
bool mqttEnable = false;
uint8_t humiditySetPoint = 0;
uint16_t waterEmptyLevel = 0;
uint16_t waterFullLevel = 0;
String mqttServer = "";
String mqttUser = "";
String mqttPassword = "";
String mqttHumidityTopic = "";

bool displayEnabled = true;
uint8_t displayLayout = DISPLAY_LAYOUT_1;

int8_t humidity = NAN;
float temperature = NAN;
uint8_t fanSpeed = 0;
uint8_t oldFanSpeed = 0;
uint8_t waterPercent = 0;
uint8_t airManAnimFrame = 0;
uint8_t airManAnimFrameCount = 0;

bool powerOn = true;
bool topOff = false;
bool topWasOff = false;
bool fanAuto = true;
bool waterEmpty = false;
bool textBlinkState = false;
bool shouldUpdateDisplay = false;
bool firmwareUpdating = false;

MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);

VL53L0X waterSensor;
Adafruit_SSD1306 display(128, 64, &Wire, -1);
Adafruit_Si7021 humiditySensor = Adafruit_Si7021();

uint8_t humiditySparkLineAddCounter = 0;
SparkLine<uint8_t> humiditySparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) {
  display.drawLine(x0, y0, x1, y1, WHITE);
});

void setup()
{
  Wire.begin();
  Wire.setClock(400000L);
  WiFiManager wifiManager;

  pinMode(PIN_FAN_MOSFET, OUTPUT);
  pinMode(PIN_HUMID_BUTTON, INPUT);
  pinMode(PIN_FAN_BUTTON, INPUT);
  analogWriteFreq(25000);

  // Turn off fan
  analogWrite(PIN_FAN_MOSFET, 0);

  // Get MAC address and build AP SSID
  uint8_t macAddr[6];
  char apName[13];
  WiFi.macAddress(macAddr);
  sprintf(apName, "humid-%02x%02x", macAddr[4], macAddr[5]);
  mdnsName = apName;

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  eepromReadSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Initialize display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.ssd1306_command(SSD1306_SETCONTRAST);
  display.ssd1306_command(0x20);
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  display.setCursor(38, 12);
  display.print("...");
  display.setTextSize(1);
  display.setCursor(3, 40);
  display.println(F("Connecting to Wifi"));
  display.display();

  // If both buttons pressed (active high), factory reset
  if (digitalRead(PIN_HUMID_BUTTON) && digitalRead(PIN_FAN_BUTTON))
  {
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    delay(3000);
    ESP.restart();
  }

  // Connect to WiFi
  wifiManager.setAPCallback(wifiConfigModeCallback);
  if (!wifiManager.autoConnect(apName))
  {
    display.setTextSize(2);
    display.setCursor(50, 0);
    display.print("!");
    display.setTextSize(1);
    display.setCursor(3, 20);
    display.print(F("Unable to connect        to Wifi!"));
    display.setCursor(12, 45);
    display.print(F("Check WiFi and         settings"));
    delay(3000);
    ESP.reset();
  }

  mdns.begin("humidifier");
  server.on("/settings", HTTP_GET, [](){ handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, saveSettings);
  server.on("/getSettings", getSettings);
  server.on("/getStatus", getStatus);
  server.on("/calib", doCalib);
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    server.send(200, "text/html", F("Clearing all saved settings (including Wifi) and restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  httpUpdater.setStartCallback(displayFirmwareUpdateStart);
  httpUpdater.setSuccessCallback(displayFirmwareUpdateSuccess);
  httpUpdater.setFailCallback(displayFirmwareUpdateFail);
  server.begin();

  humiditySensor.begin();

  waterSensor.init();
  //waterSensor.setMeasurementTimingBudget(50000);
  waterSensor.setTimeout(500);
  measureWaterLevel();

  // Setup MQTT, if enabled
  if (mqttEnable)
  {
    mqtt.setServer(mqttServer.c_str(), 1883);
    mqtt.setCallback(mqttCallback);
    mqtt.setBufferSize(1024);
    mqttReconnect();
  }

  // Show that we're connected
  IPAddress ip = WiFi.localIP();
  display.clearDisplay();

  // draw lazy checkmark
  display.setTextSize(2);
  display.setCursor(47, 5);
  display.print("\\");
  display.setCursor(55, 5);
  display.print("/");
  display.fillRect(47, 5, 4, 11, BLACK);

  display.setTextSize(1);
  display.setCursor(3, 25);
  display.println(F("Connected to Wifi!"));
  display.setCursor(0, 40);
  display.print(String(ip[0])); display.print("."); display.print(String(ip[1])); display.print("."); display.print(String(ip[2])); display.print("."); display.print(String(ip[3]));
  display.setCursor(0, 50);
  display.print(F("humidifier.local"));
  display.display();
  delay(3000);
}

void loop()
{
  static int32_t fanRampTick = 0;
  static int32_t lastButtonTick = 0;
  static int32_t last5SecTick = 0;
  static int32_t lastAirManAnimTick = 0;
  static int32_t lastTextBlinkTick = 0;
  static int32_t lastDisplayJumbleTick = 0;
  static int32_t lastMQTTUpdateTick = 0;

  // Handle HTTP
  server.handleClient();

  // If updating firmware, don't do anything else
  if (firmwareUpdating) return;

  // Handle MQTT, if enabled
  if (mqttEnable)
  {
    if (!mqtt.connected()) mqttReconnect();
    if (mqtt.connected())
    {
      if (millis() - lastMQTTUpdateTick > MQTT_UPDATE_TIME)
      {
        mqttUpdate();
        lastMQTTUpdateTick = millis();
      }
      mqtt.loop();
    }
  }

  // Handle buttons
  if (millis() - lastButtonTick > BUTTON_POLL_TIME)
  {
    handleButtons();
    lastButtonTick = millis();
  }

  // If we're not powered on, nothing else to do!
  if (!powerOn) return;

  // If the top is off, temporarily shut everything off until it's back on
  if (topOff)
  {
    if (!topWasOff)
    {
      fanSpeed = 0;
      oldFanSpeed = 255;
      analogWrite(PIN_FAN_MOSFET, 0);

      display.clearDisplay();
      display.setTextSize(2);
      display.setCursor(50, 5);
      display.print("!");
      display.setTextSize(1);
      display.setCursor(34, 25);
      display.print(F("Top Off!"));
      display.setCursor(5, 35);
      display.print(F(" Press any button    after top back on        to resume"));
      display.display();

      topWasOff = true;
    }
    return;
  }

  // Jumble around display layout to prevent burn in
  if (millis() - lastDisplayJumbleTick > DISPLAY_JUMBLE_TIME)
  {
    if (++displayLayout >= NUM_DISPLAY_LAYOUTS) displayLayout = DISPLAY_LAYOUT_1;
    lastDisplayJumbleTick = millis();
    shouldUpdateDisplay = true;
  }

  // Do every 5 seconds...
  if (millis() - last5SecTick > 5000)
  {
    measureWaterLevel();

    float retVal = humiditySensor.readHumidity();
    if (retVal != NAN)
    {
      humidity = (int8_t)retVal;

      // Add humidity value to sparkline every minute
      if (++humiditySparkLineAddCounter > 6)
      {
        humiditySparkLine.add(humidity);
        humiditySparkLineAddCounter = 0;
      }
    }

    retVal = humiditySensor.readTemperature();
    if (retVal != NAN) temperature = retVal;

    shouldUpdateDisplay = true;
    last5SecTick = millis();
  }

  // Only update display when necessary
  if (displayEnabled)
  {
    bool doUpdate = false;

    if (shouldUpdateDisplay) doUpdate = true;

    if (fanSpeed > 0 && (millis() - lastAirManAnimTick > airManAnimSpeeds[fanSpeed]))
    {
      if (++airManAnimFrameCount >= 4) airManAnimFrameCount = 0;

      if (fanSpeed < 2) airManAnimFrame = airManSlowerAnim[airManAnimFrameCount];
      else if (fanSpeed < 3) airManAnimFrame = airManSlowAnim[airManAnimFrameCount];
      else if (fanSpeed < 4) airManAnimFrame = airManMedAnim[airManAnimFrameCount];
      else airManAnimFrame = airManFastAnim[airManAnimFrameCount];

      lastAirManAnimTick = millis();
      doUpdate = true;
    }

    if (millis() - lastTextBlinkTick > 1000)
    {
      textBlinkState = !textBlinkState;
      lastTextBlinkTick = millis();
      doUpdate = true;
    }

    if (doUpdate) displayUpdate();
  }

  // If we don't have a valid humidity reading, we can't really do anything else
  if (humidity == NAN) return;

  // Set fan speed according to humidity set point
  // Ramp up fan if humidity keeps dropping, but keep it at that speed fixed until it recovers
  if (fanAuto)
  {
    uint8_t newFanSpeed = 0;

    if (waterEmpty) fanSpeed = 1;
    else if (humidity >= (humiditySetpoints[humiditySetPoint] + 3))  fanSpeed = 0;
    else if (humidity >=  humiditySetpoints[humiditySetPoint])       fanSpeed = 1;
    else if (humidity >= (humiditySetpoints[humiditySetPoint] - 2))  newFanSpeed = 3;
    //else if (humidity >= (humiditySetpoints[humiditySetPoint] - 3))  newFanSpeed = 4;
    else if (humidity >= (humiditySetpoints[humiditySetPoint] - 5))  newFanSpeed = 4;
    else newFanSpeed = 5;

    if (newFanSpeed > fanSpeed) fanSpeed = newFanSpeed;
  }

  // Set fan speed. Ramp fan on speed increase
  if (fanSpeed != oldFanSpeed)
  {
    if (millis() - fanRampTick > FAN_RAMP_STEP_TIME)
    {
      if (fanSpeed > oldFanSpeed) oldFanSpeed++;
      else oldFanSpeed = fanSpeed;

      analogWrite(PIN_FAN_MOSFET, fanSpeeds[oldFanSpeed]);
      fanRampTick = millis();
    }
    shouldUpdateDisplay = true;
  }

  // If fan speed changes, update MQTT
  if ((!oldFanSpeed && fanSpeed) || (!fanSpeed && oldFanSpeed)) mqttUpdate();
}

void handleButtons()
{
  #define BUTTON_DEBOUNCE_COUNT   3

  static int8_t humidButtonDebounceCount = 0;
  static int8_t fanButtonDebounceCount = 0;

  static bool humidButtonDebounceState = false;
  static bool fanButtonDebounceState = false;
  static bool humidButtonIsPressed = false;
  static bool fanButtonIsPressed = false;
  static bool humidButtonProcessed = false;
  static bool fanButtonProcessed = false;

  static uint32_t humidButtonPressTime = 0;
  static uint32_t fanButtonPressTime = 0;

  // Button debounce
  humidButtonDebounceCount += (digitalRead(PIN_HUMID_BUTTON) ? 1 : -1);
  if (humidButtonDebounceCount > BUTTON_DEBOUNCE_COUNT) humidButtonDebounceState = true;
  else if (humidButtonDebounceCount < 0) humidButtonDebounceState = false;
  humidButtonDebounceCount = constrain(humidButtonDebounceCount, 0, BUTTON_DEBOUNCE_COUNT);

  fanButtonDebounceCount += (digitalRead(PIN_FAN_BUTTON) ? 1 : -1);
  if (fanButtonDebounceCount > BUTTON_DEBOUNCE_COUNT) fanButtonDebounceState = true;
  else if (fanButtonDebounceCount < 0) fanButtonDebounceState = false;
  fanButtonDebounceCount = constrain(fanButtonDebounceCount, 0, BUTTON_DEBOUNCE_COUNT);

  // Process button presses
  if (humidButtonDebounceState)
  {
    // Humidity button just pressed
    if (!humidButtonIsPressed)
    {
      humidButtonPressTime = millis();
      humidButtonIsPressed = true;
    }
    else
    {
      // If humidity button held for BUTTON_LONG_PRESS_TIME...
      if (!humidButtonProcessed && (millis() - humidButtonPressTime > BUTTON_LONG_PRESS_TIME))
      {
        displayEnabled = false;
        display.clearDisplay();
        display.display();
        mqttUpdate();
        humidButtonProcessed = true;
      }
    }
  }
  // Humidity button just released
  else if (humidButtonIsPressed)
  {
    // Humidity button short press
    if (!humidButtonProcessed)
    {
      if (powerOn)
      {
        if (!displayEnabled) displayEnabled = true;
        if (topOff) topOff = false;
        else
        {
          if (++humiditySetPoint >= NUM_HUMIDITY_SETPOINTS) humiditySetPoint = 0;
          fanSpeed = 0; // force main loop to pick new, correct speed
          oldFanSpeed = 255;
        }
        shouldUpdateDisplay = true;
        mqttUpdate();
      }
    }
    humidButtonIsPressed = false;
    humidButtonProcessed = false;
  }

  if (fanButtonDebounceState)
  {
    // Fan buton just pressed
    if (!fanButtonIsPressed)
    {
      fanButtonPressTime = millis();
      fanButtonIsPressed = true;
    }
    else
    {
      // If fan button held for BUTTON_LONG_PRESS_TIME...
      if (!fanButtonProcessed && (millis() - fanButtonPressTime > BUTTON_LONG_PRESS_TIME))
      {
        if (powerOn) setPower(false);
        else setPower(true);
        mqttUpdate();
        fanButtonProcessed = true;
      }
    }
  }
  // Fan button just released
  else if (fanButtonIsPressed)
  {
    // Fan button short press
    if (!fanButtonProcessed)
    {
      if (powerOn)
      {
        if (!displayEnabled) displayEnabled = true;
        if (topOff) topOff = false;
        else
        {
          if (fanAuto)
          {
            fanAuto = false;
            fanSpeed = 0; // force main loop to pick new, correct speed
            oldFanSpeed = 255;
          }
          else if (++fanSpeed >= NUM_FAN_SPEEDS)
          {
            fanAuto = true;
            fanSpeed = 0;
            oldFanSpeed = 255;
          }
        }
        mqttUpdate();
      }
    }
    shouldUpdateDisplay = true;
    fanButtonIsPressed = false;
    fanButtonProcessed = false;
  }
}

void doCalib()
{
  char buffer[6] = { 0 };

  if (server.hasArg("empty"))
  {
    waterEmptyLevel = waterSensor.readRangeSingleMillimeters();
    if (!waterSensor.timeoutOccurred())
    {
      eepromWriteU16(SETTING_WATER_EMPTY, waterEmptyLevel);
      EEPROM.commit();
      sprintf(buffer, "%d", waterEmptyLevel);
      server.send(200, "text/plain", buffer);
    }
    else
    {
      waterEmptyLevel = 0;
      server.send(400);
    }
  }
  else if (server.hasArg("full"))
  {
    waterFullLevel = waterSensor.readRangeSingleMillimeters();
    if (!waterSensor.timeoutOccurred())
    {
      eepromWriteU16(SETTING_WATER_FULL, waterFullLevel);
      EEPROM.commit();
      sprintf(buffer, "%d", waterFullLevel);
      server.send(200, "text/plain", buffer);
    }
     else
     {
       waterFullLevel = 0;
       server.send(400);
     }
  }
  else if (server.hasArg("setEmpty"))
  {
    waterEmptyLevel = server.arg("setEmpty").toInt();
    eepromWriteU16(SETTING_WATER_EMPTY, waterEmptyLevel);
    EEPROM.commit();
    sprintf(buffer, "%d", waterEmptyLevel);
    server.send(200, "text/plain", buffer);
  }
  else if (server.hasArg("setFull"))
  {
    waterFullLevel = server.arg("setFull").toInt();
    eepromWriteU16(SETTING_WATER_FULL, waterFullLevel);
    EEPROM.commit();
    sprintf(buffer, "%d", waterFullLevel);
    server.send(200, "text/plain", buffer);
  }
  else server.send(400);
}

void displayUpdate()
{
  if (!displayEnabled) return;

  char buffer[16] = { 0 };
  char humidText[4] = { 0 };

  if (humidity > 0) sprintf(humidText, (fanAuto ? "%2d" : "%2d%%"), humidity);
  else sprintf(humidText, (fanAuto ? "--" : "--%%"));

  uint8_t waterBarWidth = 46 * waterPercent / 100;

  display.clearDisplay();
  display.setTextSize(1);

  switch (displayLayout)
  {
    case DISPLAY_LAYOUT_1:
      display.setCursor(0, 0);
      display.print("Humidity");
      if (fanAuto)
      {
        display.setCursor(24, 17);
        sprintf(buffer, "/%d%%", humiditySetpoints[humiditySetPoint]);
        display.print(buffer);
      }
      display.setCursor(45, 32);
      display.print("Fan");
      display.setFont(&TomThumb);
      display.setCursor(45, 64);
      display.print((fanAuto ? "Auto" : "Manual"));
      display.setFont(NULL);
      display.setCursor(75, 32);
      if (waterPercent < LOW_WATER_WARNING && textBlinkState) display.setTextColor(BLACK, WHITE);
      display.print("Water");
      display.setTextColor(WHITE);
      display.setTextSize(2);
      display.setCursor(0, 10);
      display.print(humidText);
      display.setCursor(45, 42);
      sprintf(buffer, "%d", fanSpeed);
      display.print(buffer);
      display.setCursor(75, 42);
      sprintf(buffer, "%d%%", waterPercent);
      if (waterPercent < LOW_WATER_WARNING && textBlinkState) display.setTextColor(BLACK, WHITE);
      display.print(buffer);
      display.setTextColor(WHITE);
      display.drawRoundRect(75, 59, 46, 5, 2, WHITE);
      display.fillRoundRect(75, 59, waterBarWidth, 5, 2, WHITE);

      if (fanSpeed == 0) display.drawBitmap(0, 32, airmanIdle1, 37, 32, WHITE);
      else
      {
        switch (airManAnimFrame)
        {
          default:
          case 0: display.drawBitmap(0, 32, airmanIdle1, 37, 32, WHITE); break;
          case 1: display.drawBitmap(0, 32, airmanIdle2, 37, 32, WHITE); break;
          case 2: display.drawBitmap(0, 32, airmanRun1, 37, 32, WHITE); break;
          case 3: display.drawBitmap(0, 32, airmanRun2, 37, 32, WHITE); break;
          case 4: display.drawBitmap(0, 32, airmanRun3, 37, 32, WHITE); break;
          case 5: display.drawBitmap(0, 32, airmanRun4, 37, 32, WHITE); break;
        }
      }

      display.setFont(&Picopixel);
      display.setTextSize(1);
      display.setCursor(115, 4);
      display.print(humiditySparkLine.findMax());
      display.setCursor(115, 23);
      display.print(humiditySparkLine.findMin());
      display.setFont(NULL);
      humiditySparkLine.draw(52, 24, 60, 24);
    break;

    case DISPLAY_LAYOUT_2:
      display.setCursor(0, 40);
      display.print("Humidity");
      if (fanAuto)
      {
        display.setCursor(24, 57);
        sprintf(buffer, "/%d%%", humiditySetpoints[humiditySetPoint]);
        display.print(buffer);
      }
      display.setCursor(45, 0);
      display.print("Fan");
      display.setFont(&TomThumb);
      display.setCursor(45, 33);
      display.print((fanAuto ? "Auto" : "Manual"));
      display.setFont(NULL);
      display.setCursor(75, 0);
      if (waterPercent < LOW_WATER_WARNING && textBlinkState) display.setTextColor(BLACK, WHITE);
      display.print("Water");
      display.setTextColor(WHITE);
      display.setTextSize(2);
      display.setCursor(0, 50);
      display.print(humidText);
      display.setCursor(45, 10);
      sprintf(buffer, "%d", fanSpeed);
      display.print(buffer);
      display.setCursor(75, 10);
      sprintf(buffer, "%d%%", waterPercent);
      if (waterPercent < LOW_WATER_WARNING && textBlinkState) display.setTextColor(BLACK, WHITE);
      display.print(buffer);
      display.setTextColor(WHITE);
      display.drawRoundRect(75, 28, 46, 5, 2, WHITE);
      display.fillRoundRect(75, 28, waterBarWidth, 5, 2, WHITE);

      if (fanSpeed == 0) display.drawBitmap(0, 0, airmanIdle1, 37, 32, WHITE);
      else
      {
        switch (airManAnimFrame)
        {
          default:
          case 0: display.drawBitmap(0, 0, airmanIdle1, 37, 32, WHITE); break;
          case 1: display.drawBitmap(0, 0, airmanIdle2, 37, 32, WHITE); break;
          case 2: display.drawBitmap(0, 0, airmanRun1, 37, 32, WHITE); break;
          case 3: display.drawBitmap(0, 0, airmanRun2, 37, 32, WHITE); break;
          case 4: display.drawBitmap(0, 0, airmanRun3, 37, 32, WHITE); break;
          case 5: display.drawBitmap(0, 0, airmanRun4, 37, 32, WHITE); break;
        }
      }

      display.setFont(&Picopixel);
      display.setTextSize(1);
      display.setCursor(115, 44);
      display.print(humiditySparkLine.findMax());
      display.setCursor(115, 63);
      display.print(humiditySparkLine.findMin());
      display.setFont(NULL);
      humiditySparkLine.draw(52, 64, 60, 24);
    break;
  }

  display.display();
  shouldUpdateDisplay = false;
}

void setPower(bool state)
{
  if (!state)
  {
    powerOn = false;
    fanSpeed = 0;
    analogWrite(PIN_FAN_MOSFET, 0);
    displayEnabled = false;
    display.clearDisplay();
    display.display();
  }
  else
  {
    powerOn = true;
    fanSpeed = 0;
    oldFanSpeed = 255;  // force fan speed update
    displayEnabled = true;
    shouldUpdateDisplay = true;
  }

  mqttUpdate();
}

void measureWaterLevel()
{
  static bool firstRange = true;
  static uint16_t prevRange = 0;
  static uint16_t rangeDebounce = 0;
  static uint16_t waterLevel = 0;

  if (waterEmptyLevel == 0 || waterFullLevel == 0)
  {
    waterPercent = 0;
    waterEmpty = true;
    return;
  }

  uint16_t range = (uint16_t)ceil((float)(waterSensor.readRangeSingleMillimeters() / 10));
  if (waterSensor.timeoutOccurred()) goto waterError;

  if (range > waterEmpty + 40)
  {
    topOff = true;
    topWasOff = false;
    firstRange = true;
    return;
  }

  if (firstRange)
  {
    waterLevel = range;
    firstRange = false;
  }
  else if (range == prevRange)
  {
    if (++rangeDebounce >= WATER_LEVEL_DEBOUNCE)
    {
      waterLevel = range;
      rangeDebounce = WATER_LEVEL_DEBOUNCE;
    }
  }
  else
  {
    prevRange = range;
    rangeDebounce = 0;
  }

  if (waterLevel >= waterEmptyLevel) waterPercent = 0;
  else if (waterLevel <= waterFullLevel) waterPercent = 100;
  else waterPercent = (((waterEmptyLevel - waterLevel) * 100) / (waterEmptyLevel - waterFullLevel));

  waterError:
    waterEmpty = (waterPercent > 0 ? false : true);
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";
  else if (path.endsWith(".js")) dataType = "text/javascript";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}

void MQTT_haDiscovery()
{
  if (!mqtt.connected()) return;

  String id;
  String dev;
  String topic;

  char buf[7];
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  sprintf(buf, "%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  id = buf;

  dev = "\"dev\": { \"name\": \"Humidifier\", \"ids\": \"humid-" + id + "\", \"mdl\": \"Humidifier\", \"mf\": \"Akudaikon\", \"cu\": \"http://" + WiFi.localIP().toString() + "\" }";

  String config;
  String availTopic;

  availTopic = "humidifier/availability";

  topic = "homeassistant/humidifier/humid-" + id + "/config";
  config = "{\"name\": \"Humidifier\", " \
            "\"dev_cla\": \"humidifier\", " \
            "\"stat_t\": \"humidifier/power/state\", " \
            "\"cmd_t\": \"humidifier/power/command\", " \
            "\"act_t\": \"humidifier/action/state\", " \
            "\"hum_stat_t\": \"humidifier/humidity/setpoint\", " \
            "\"hum_cmd_t\": \"humidifier/humidity/setpoint/command\", " \
            "\"current_humidity_topic\": \"humidifier/humidity/percent\", " \
            "\"modes\": [\"auto\", \"5\", \"4\", \"3\", \"2\", \"1\"], " \
            "\"mode_stat_t\": \"humidifier/mode/state\", " \
            "\"mode_cmd_t\": \"humidifier/mode/command\", " \
            "\"avty_t\": \"humidifier/availability\", "\
            "\"min_hum\": 25, " \
            "\"max_hum\": 65, " \
            "\"uniq_id\": \"humid-" + id + "\", " + dev + " }";
  mqtt.publish(topic.c_str(), config.c_str(), true);

  topic = "homeassistant/sensor/humid-" + id + "-water/config";
  config = "{\"name\": \"Water Level\", \"obj_id\": \"humidifier_water_level\", \"ic\": \"mdi:cup-water\", \"stat_t\": \"humidifier/water/percent\", \"avty_t\": \"" + availTopic + "\", \"unit_of_meas\": \"%\", \"uniq_id\": \"" + id + "-water\", " + dev + " }";
  mqtt.publish(topic.c_str(), config.c_str(), true);

  topic = "homeassistant/switch/humid-" + id + "-display/config";
  config = "{\"name\": \"Display\", \"obj_id\": \"humidifier_display\", \"ic\": \"mdi:lightbulb-on-outline\", \"stat_t\": \"humidifier/display/state\", \"cmd_t\": \"humidifier/display/command\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-display\", " + dev + " }";
  mqtt.publish(topic.c_str(), config.c_str(), true);
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    if (mqtt.connect(mdnsName.c_str(), (mqttUser == "" ? NULL : mqttUser.c_str()), (mqttPassword == "" ? NULL : mqttPassword.c_str()), "humidifier/availability", 1, true, "offline"))
    {
      mqtt.publish("humidifier/availability", "online", true);

      String topic = "humidifier/power/command";
      mqtt.subscribe(topic.c_str());

      topic = "humidifier/mode/command";
      mqtt.subscribe(topic.c_str());

      topic = "humidifier/display/command";
      mqtt.subscribe(topic.c_str());

      topic = "humidifier/humidity/setpoint/command";
      mqtt.subscribe(topic.c_str());

      MQTT_haDiscovery();
      mqttUpdate();
      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void mqttUpdate()
{
  if (!mqtt.connected()) return;

  char buffer[8] = { 0 };

  String topic = "humidifier/power/state";
  if (powerOn) mqtt.publish(topic.c_str(), "ON", true);
  else mqtt.publish(topic.c_str(), "OFF", true);

  topic = "humidifier/action/state";
  if (powerOn)
  {
    if (humidity < humiditySetpoints[humiditySetPoint]) mqtt.publish(topic.c_str(), "humidifying", true);
    else mqtt.publish(topic.c_str(), "idle", true);
  }
  else mqtt.publish(topic.c_str(), "off", true);

  topic = "humidifier/mode/state";
  if (powerOn)
  {
    sprintf(buffer, "%d", fanSpeed);
    if (fanAuto) mqtt.publish(topic.c_str(), "auto", true);
    else mqtt.publish(topic.c_str(), buffer, true);
  }

  topic = "humidifier/display/state";
  if (displayEnabled) mqtt.publish(topic.c_str(), "ON", true);
  else mqtt.publish(topic.c_str(), "OFF", true);

  if (humidity != NAN)
  {
    topic = "humidifier/humidity/percent";
    sprintf(buffer, "%d", humidity);
    mqtt.publish(topic.c_str(), buffer, false);
  }

  topic = "humidifier/humidity/setpoint";
  sprintf(buffer, "%d", humiditySetpoints[humiditySetPoint]);
  mqtt.publish(topic.c_str(), buffer, false);

  topic = "humidifier/water/percent";
  sprintf(buffer, "%d", waterPercent);
  mqtt.publish(topic.c_str(), buffer, false);
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  if (!strcmp(topic, "humidifier/power/command"))
  {
    if (!strncasecmp_P((char*)payload, "on", length)) setPower(true);
    else if (!strncasecmp_P((char*)payload, "off", length)) setPower(false);
    mqttUpdate();
  }
  else if (!strcmp(topic, "humidifier/display/command"))
  {
    if (!strncasecmp_P((char*)payload, "on", length))
    {
      displayEnabled = true;
      shouldUpdateDisplay = true;
    }
    else if (!strncasecmp_P((char*)payload, "off", length))
    {
      displayEnabled = false;
      display.clearDisplay();
      display.display();
    }
    mqttUpdate();
  }
  else if (!strcmp(topic, "humidifier/mode/command"))
  {
    if (!strncasecmp_P((char*)payload, "auto", length))
    {
      fanAuto = true;
      fanSpeed = 0; // force main loop to pick new, correct speed
      oldFanSpeed = 255;
    }
    else if (!strncasecmp_P((char*)payload, "5", length))
    {
      fanAuto = false;
      fanSpeed = 5;
      oldFanSpeed = 255;
    }
    else if (!strncasecmp_P((char*)payload, "4", length))
    {
      fanAuto = false;
      fanSpeed = 4;
      oldFanSpeed = 255;
    }
    else if (!strncasecmp_P((char*)payload, "3", length))
    {
      fanAuto = false;
      fanSpeed = 3;
      oldFanSpeed = 255;
    }
    else if (!strncasecmp_P((char*)payload, "2", length))
    {
      fanAuto = false;
      fanSpeed = 2;
      oldFanSpeed = 255;
    }
    else if (!strncasecmp_P((char*)payload, "1", length))
    {
      fanAuto = false;
      fanSpeed = 1;
      oldFanSpeed = 255;
    }
    mqttUpdate();
  }
  else if (!strcmp(topic, "humidifier/humidity/setpoint/command"))
  {
    uint8_t percent = atoi((char*)payload);
    if (percent >= 25 && percent <= 65)
    {
      percent = (percent / 5) - 5;
      humiditySetPoint = percent;
      fanSpeed = 0; // force main loop to pick new, correct speed
      oldFanSpeed = 255;
      mqttUpdate();
    }
  }
}

void getSettings()
{
  DynamicJsonDocument root(200);

  root["mqttEnable"] = mqttEnable;
  root["humiditySetPoint"] = humiditySetPoint;
  root["waterEmpty"] = waterEmptyLevel;
  root["waterFull"] = waterFullLevel;
  root["mdnsName"] = mdnsName;
  root["mqttServer"] = mqttServer;
  root["mqttUser"] = mqttUser;
  root["mqttPassword"] = mqttPassword;

  String buffer;
  serializeJson(root, buffer);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/json", buffer);
}

void getStatus()
{
  DynamicJsonDocument root(200);

  root["powerOn"] = powerOn;
  root["displayOn"] = displayEnabled;
  root["fanSpeed"] = fanSpeed;
  root["fanAuto"] = fanAuto;
  root["humidity"] = humidity;
  root["humiditySetPoint"] = humiditySetPoint;
  root["waterPercent"] = waterPercent;

  String buffer;
  serializeJson(root, buffer);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/json", buffer);
}

void saveSettings()
{
  String response;

  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    eepromWriteString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttEnable"))
  {
    mqttEnable = (server.arg("mqttEnable") == "on" ? true : false);
    EEPROM.write(SETTING_MQTT_ENABLE, mqttEnable);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    eepromWriteString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    eepromWriteString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    eepromWriteString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  EEPROM.commit();

  response = F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(1000);
  ESP.reset();
}

void eepromInitSettings()
{
  EEPROM.write(SETTING_INITIALIZED,   SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_MQTT_ENABLE,              0);  // Enable MQTT
  EEPROM.write(SETTING_HUMIDITY_SETPOINT,        4);  // Humidity setpoint
  eepromWriteU16(SETTING_WATER_EMPTY,            0);  // Water empty measurement
  eepromWriteU16(SETTING_WATER_FULL,             0);  // Water full measurement
  eepromWriteString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  eepromWriteString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  eepromWriteString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  eepromWriteString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM.commit();
}

void eepromReadSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) eepromInitSettings();

  mdnsName = eepromReadString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  mqttEnable = EEPROM.read(SETTING_MQTT_ENABLE);
  humiditySetPoint = EEPROM.read(SETTING_HUMIDITY_SETPOINT);
  waterEmptyLevel = eepromReadU16(SETTING_WATER_EMPTY);
  waterFullLevel = eepromReadU16(SETTING_WATER_FULL);
  mqttServer = eepromReadString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = eepromReadString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = eepromReadString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

String eepromReadString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void eepromWriteString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

int16_t eepromReadU16(uint16_t startAddress)
{
  uint8_t value[2] = { 0 };
  uint16_t retVal = 0;

  for (int i = 0; i < 2; i++) value[i] = EEPROM.read(startAddress + i);

  memcpy(&retVal, value, sizeof(retVal));
  return retVal;
}

void eepromWriteU16(uint16_t startAddress, uint16_t value)
{
  for (int i = 0; i < 2; i++) EEPROM.write(startAddress + i, ((uint8_t*)&value)[i]);
}

void wifiConfigModeCallback(WiFiManager *myWiFiManager)
{
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  display.setCursor(50, 5);
  display.print("!");
  display.setTextSize(1);
  display.setCursor(25, 25);
  display.print(F("Connect to"));
  display.setTextSize(2);
  display.setCursor(0, 35);
  display.print(mdnsName);
  display.setTextSize(1);
  display.setCursor(6, 52);
  display.print(F("to configure Wifi"));
  display.display();
}

void displayFirmwareUpdateStart()
{
  analogWrite(PIN_FAN_MOSFET, 0);
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(38, 12);
  display.print("...");
  display.setTextSize(1);
  display.setCursor(6, 40);
  display.println(F("Updating Firmware"));
  display.display();
  firmwareUpdating = true;
}

void displayFirmwareUpdateSuccess()
{
  display.clearDisplay();

  // draw lazy checkmark
  display.setTextSize(2);
  display.setCursor(47, 10);
  display.print("\\");
  display.setCursor(55, 10);
  display.print("/");
  display.fillRect(47, 10, 4, 16, BLACK);

  display.setTextSize(1);
  display.setCursor(12, 30);
  display.print(F("Firmware Update"));
  display.setCursor(23, 45);
  display.print(F("Successful!"));
  display.display();
}

void displayFirmwareUpdateFail()
{
  display.setTextSize(2);
  display.setCursor(50, 10);
  display.print("!");
  display.setTextSize(1);
  display.setCursor(12, 30);
  display.print(F("Firmware Update"));
  display.setCursor(38, 45);
  display.print(F("Failed"));
  display.display();
  firmwareUpdating = false;
}
