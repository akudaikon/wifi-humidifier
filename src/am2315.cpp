#include <Arduino.h>
#include <Wire.h>
#include "am2315.h"

static float humidity = -1;
static float temperature = -1;

uint32_t delayByCPU(uint32_t delaycount)
{
  // Do nonsense to force a blocking delay time (delay() will not actually block but let wifi stuff happen)
  // Math is nonsense to force compiler to not optimize it out
  #define COUNT 100000

  float result = 0;

  for (uint32_t i = 0; i < (delaycount * 166); i++)
  {
    for (uint32_t j = 0; j < 1000; j++)
    {
      for (uint32_t index = 0; index < COUNT; index++)
      {
        result = 30.4 + i;
        result = result / 50.0;
      }
      result = result + j;
    }
  }

  return (uint32_t)result;
}

bool AM2315_readData()
{
  uint8_t data[8];

  noInterrupts();
  ETS_INTR_LOCK();

  // wake up the sensor
  Wire.beginTransmission(AM2315_I2C_ADDR);
  Wire.write(AM2315_READREG);
  delayByCPU(50);
  Wire.endTransmission();

  // request data
  Wire.beginTransmission(AM2315_I2C_ADDR);
  Wire.write(AM2315_READREG);
  Wire.write(0x00); // start at address 0x0
  Wire.write(4);    // request 4 bytes data
  Wire.endTransmission();

  delayByCPU(50);

  // read data
  Wire.requestFrom(AM2315_I2C_ADDR, 8);
  for (uint8_t i = 0; i < 8; i++) data[i] = Wire.read();

  interrupts();
  ETS_UART_INTR_ENABLE();
  yield();

  if (data[0] != AM2315_READREG) return false;
  if (data[1] != 4) return false; // bytes req'd

  humidity = data[2];
  humidity *= 256;
  humidity += data[3];
  humidity /= 10;

  temperature = data[4] & 0x7F;
  temperature *= 256;
  temperature += data[5];
  temperature /= 10;
  if (data[4] >> 7) temperature = -temperature;

  return true;
}

float AM2315_getHumidity()
{
  return humidity;
}

float AM2315_getTemperature()
{
  return (temperature * 1.8) + 32;  // to degF
}
